package detection

import (
	"fmt"

	"github.com/labstack/gommon/log"
	"github.com/sclevine/agouti"
)

const (
	// チェックするウェブサイトURLを記述
	URL = "http://www.aaaa.com"
)

func Check() error {
	var (
		err      error
		id       = "loginID"
		password = "password"
	)

	// ウェブドライバーを起動する
	agoutiDriver := agouti.PhantomJS()
	agoutiDriver.Start()
	defer agoutiDriver.Stop()
	page, _ := agoutiDriver.NewPage()

	// 自動操作開始
	if err = page.Navigate(fmt.Sprintf("%s/login", URL)); err != nil {
		log.Error("アクセスできず")
	} else {
		log.Info("アクセス")
	}

	// 適宜フォームnameにアカウント情報を流し込み、ログイン
	page.FindByName("id").Fill(id)
	page.FindByName("password").Fill(password)
	if err = page.FirstByClass("sending").Click(); err != nil {
		log.Error("ログインできず")
	} else {
		log.Info("ログイン完了")
	}

	// ここでなにかを監視する
	// span.thisにテキストがなければ、エラーを返す
	if _, err := page.Find("div.test > span.this").Text(); err != nil {
		// 更新出来ませんのテキストがなかったので、sendmainで通知
		return err
	}

	// エラーが出なければ、何も返さない
	return nil
}
