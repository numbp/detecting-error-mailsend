package main

import (
	"log"
	"net/smtp"
	"os"
	"time"

	d "gitlab.com/k-terashima/webdriver/go-selenium-error-send.pub/detect"
)

type mail struct {
	from     string
	username string
	password string
	to       string
	sub      string
	msg      string
}

// gmailの場合は2段階認証を解除し
// Google設定「安全性の低いアプリの許可」を有効にする必要があり
// セキュリティが甘くなるため、送信用メールアカウントがあると便利
func gmailSend(m mail) error {
	smtpSvr := "smtp.gmail.com:587"
	auth := smtp.PlainAuth("", m.username, m.password, "smtp.gmail.com")
	if err := smtp.SendMail(smtpSvr, auth, m.from, []string{m.to}, []byte(m.body())); err != nil {
		return err
	}
	return nil
}

func main() {
	var err error

	h1 := time.NewTicker(time.Hour * 1)

	// 無限ループで1時間に一度チェックしに行く
	// forといっても処理がないので軽いよ
	for {
		// switchだと動かないよ
		select {
		case <-h1.C:
			err = d.Check()
			// エラーを検出したらメールを送る
			if err != nil {
				m := mail{
					// 通知送信用メアドから送信する
					from:     "送信用メールアドレス@gmail.com",
					username: "送信用メールアドレスアカウント名@gmail.com",
					password: "上記メアド認証用パスワード",
					to:       "送信先",
					sub:      "メールタイトル: xxxがクラッシュ",
					msg:      "本文: \nで改行",
				}

				// gmailからメールを送っちゃうよ
				if err := gmailSend(m); err != nil {
					log.Println(err)
					os.Exit(1)
				}
			} else {
				log.Println("成功")
			}
		}
	}

	defer h1.Stop()
}

// 送信dataのリターン
func (m mail) body() string {
	return "To: " + m.to + "\r\n" +
		"Subject: " + m.sub + "\r\n\r\n" +
		m.msg + "\r\n"
}
